#testload.nim
import os, tables, streams, strutils, random
import yaml, docopt

randomize()

let ver = "1.0.3"

let doc = """
testload.
  Configurable processor load for mynimall effort.

Usage:
  testload <project> [--lines=<count>] [--time=<seconds>]
  testload (-h | --help)
  testload --version

Options:
  -d --time=<seconds>     Run for <seconds> [default: 1].
  -l --lines=<count>      Generate <count> lines [default: 1].
  -h --help               Show this screen.
  -v --version            Show version.
  
  """
  
let args = docopt(doc, version = ver)
let proj = $args["<project>"]
let count = parseInt($args["--lines"])
let time = parseInt($args["--time"])
let timems = if time == 0:
      random(10) * 5 * 1000
      else: time * 1000
echo $count
echo $timems

type
    TestData = OrderedTable[string,string]

var testResults: TestData
testResults = {"hostCPU": hostCPU}.toOrderedTable
testResults.add("hostOS", hostOS)
testResults.add("lines", $count)
testResults.add("msecs", $timems)

proc getTestResult(): string =
    var stuff = ""
    for index in countup(1,count):
        stuff.add($random(100) & "\n")
        # if index mod 1000 == 0:
        #   echo index
    sleep(timems)
    return stuff

var s = newFileStream(proj & ".txt", fmWrite)
testResults.dump s
s.close()

writeFile(proj & ".csv", getTestResult())
