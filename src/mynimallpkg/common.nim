#common.nim
import os, streams, osproc, random, nativesockets #, times
import yaml, stopwatch


randomize()

const versionTag* = "v0.0.7"
const mynPrefix* = "myn-"
const respath* = "../../res" / hostCPU / hostOS
const trunkBranchName* = "trunk" #fossil default? don't change wantonly
const packagesBranchName* = "packages"
const rootTag* = "ROOT"
var hostBranchName* = getHostname()
var sessionId* = ""
var projectName* = ""
var coreProjectPath* = ""
var userProjectPath* = ""
var configPath* = ""
var trunkPath* = ""
var packagesPath* = ""
var hostnamePath* = ""
var repoDB* = ""
var stateFile* = ""
var syncUrl* = ""

#--- DISPLAY VERBOSITY

var verbosityLevel: int
# error = 0, state = 1, activity = 2, debug = 3
proc setVerbosity*(verbosity = 1) =
    verbosityLevel=verbosity

proc see*(level = 0, message: string) =
    if level <= verbosityLevel:
        echo message

#--- VERSIONS
type #hold related build version info
    ConstVersion* = object
        name,tag,date,time,vcs,extra: string

proc newConstVersion*(name, tag, date="", time="", vcs="", extra=""): ConstVersion =
    ConstVersion(name:name, tag:tag, date:date, time:time, vcs:vcs, extra:extra);
        
type
    Version = tuple[name,tag,date,time,vcs,extra: string]

proc newVersion*(ver: ConstVersion): Version =
    result = (name:ver.name,
    tag:ver.tag,
    date:ver.date, 
    time:ver.time, 
    vcs:ver.vcs, 
    extra:ver.extra)

type
    Versions* = seq[Version]

const VcsInfo = staticExec("git rev-parse HEAD")
const thisVersion* = newConstVersion(
    name="mynimall",
    tag=versionTag,
    date=CompileDate,
    time=CompileTime,
    vcs=VcsInfo[0..7],
    extra="nim ver: " & NimVersion)

proc newVersions*(): Versions =
    result = @[newVersion(thisVersion)]

var exeVersions* = newVersions()

#-------- common procs
proc newID*(): string = 
    let chars = "0123456789ABCDEF"
    result = "" #$getTime().toSeconds()
    for i in chars:
        result.add chars[random(chars.len)]
    #result.replace(".")

proc setGlobals*(project: string, url = "") =
    syncUrl = url
    sessionId = newID()
    projectName = project
    userProjectPath = getCurrentDir()
    let cfg = getHomeDir() / ".mynimall"
    configPath = unixToNativePath cfg
    discard configPath.existsOrCreateDir
    assert existsDir configPath
    coreProjectPath = configPath / projectName
    discard coreProjectPath.existsOrCreateDir
    trunkPath = coreProjectPath / trunkBranchName
    discard trunkPath.existsOrCreateDir
    hostnamePath = coreProjectPath / hostBranchName
    packagesPath = coreProjectPath / packagesBranchName
    stateFile = trunkPath / "SystemMapState.yaml"
    repoDB = configPath / projectName & ".fsl"
    2.see "\n---\ncore project path: " & coreProjectPath
    2.see "user project path: " & userProjectPath
    2.see "db: " & repoDB
    
    
proc runProcess*(cmd: string; wrkdir: string = ""): TaintedString  =
    var timer = stopwatch(false)
    timer.start
    4.see cmd
    let curdir = getCurrentDir()
    5.see curdir
    5.see wrkdir
    if wrkdir != "":
        setCurrentDir wrkdir
    let (arOut, arErr) = execCmdEx cmd
    if arErr != 0:
        0.see "failed: " & arOut & "\n" & getCurrentDir()
    assert arErr == 0
    setCurrentDir curdir
    timer.stop
    3.see "(msec) " & $timer.msecs
    return arOut




