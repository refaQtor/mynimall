#states.nim
import os, streams, osproc, tables
import yaml, stopwatch
import common
import fossil as vcs

type
    JobState = enum
        jsQueued, jsReserved, jsActive, jsComplete

type
    WorkerState = enum
        wsActive, wsIdle

#--- PERFORMANCE
type
    Performance* = ref object of RootObj
        events, elapsed, average: int64 

proc newPerformance*(): Performance =
    new(result)
    result.events = 0
    result.elapsed = 0
    result.average = 0

proc setAveragePerformance(perf: var Performance) =
    perf.average = perf.elapsed div perf.events

proc updatePerformance(perf: var Performance, eventTime: int64) =
    perf.events.inc
    perf.elapsed = perf.elapsed + eventTime
    perf.setAveragePerformance
    
proc testPerformance(events: int32 = 10000): Performance =
    new(result)
    0.see "Machine Performance:"
    var timer = stopwatch(false)
    timer.start
    for index in countup(0,events):
        discard events * index
    timer.stop
    result.events = events
    result.elapsed = timer.usecs
    result.average = result.elapsed div result.events
    0.see " - loops = " & $result.events
    0.see " - time  = " & $result.elapsed

type
    Performances* = OrderedTableRef[string,Performance]

proc newPerformances(): Performances =
    result = {"initialize": testPerformance()}.newOrderedTable

# --- JOBS
type
    Job* = ref object of RootObj
        id*: string
        state: JobState
        queue, machineId, workerId, command, result: string
        elapsed: int64

type
    Jobs* = OrderedTableRef[string,Job]

proc newJob*(queue = "", command = ""): Job =
    new(result)
    let newid = newID() 
    result.id = newid
    result.state = JobState.jsQueued
    result.queue = queue
    result.machineId = ""
    result.workerId = ""
    result.command = command
    result.result = ""
    result.elapsed = 0
    
proc getId*(job: Job): string =
    return job.id
    
proc newJobs(): Jobs =
    result = {"bogus": newJob()}.newOrderedTable

# --- WORKERS
type
    Worker* = ref object of RootObj 
      id, jobId: string
      state: WorkerState

proc newWorker(id: string): Worker =
    new(result)
    result.id = id
    result.jobId = ""
    result.state = wsIdle

proc getId*(worker: Worker): string =
    return worker.id

type
    Workers* = OrderedTableRef[string,Worker]

proc newWorkers*(): Workers =
    result = {"1": newWorker("1")}.newOrderedTable
    var allotment = countProcessors()
    for index in countup(0,allotment):
        result[$index] = newWorker($index)

#--- MACHINES
type
    Machine* = ref object of RootObj 
        id, session, os, arch:string
        versions: Versions
        workers: Workers
        perf: Performance

type
    Machines* = OrderedTableRef[string,Machine]

proc thisMachine*(): Machine =
    new(result)
    result.id = hostBranchName
    result.session = sessionId
    result.os = hostOS
    result.arch = hostCPU
    result.versions = exeVersions
    result.workers = newWorkers()
    result.perf = testPerformance()
    
proc newMachines(): Machines =
    result = {hostBranchName: thisMachine()}.newOrderedTable

#--- SYSTEM
type
    SystemMap* =  ref object of RootObj
        id*: string
        jobs*: Jobs
        machines*: Machines
        performance*: Performances

proc newSystemMap*(): SystemMap =
    result = new(SystemMap)
    result.id = newID()
    result.jobs = newJobs()
    result.machines = newMachines()
    result.performance = newPerformances()
    5.see repr(result)

#-- PROCESSING
proc storeState*(system: SystemMap, message: string) =
    4.see "STORE MESSAGE: " & message
    5.see repr(system)
    var s = newFileStream(stateFile, fmWrite)
    system.dump s
    assert stateFile.existsFile
    s.close()
    trunkPath.vcsCommit message
    sleep(500) #//TODO: refactor to calculated value from run perf
     
proc loadState*(): SystemMap =
    assert stateFile.existsFile
    var s = newFileStream(stateFile, fmRead)
    var system: SystemMap
    s.load system
    s.close()
    return system

proc stateFileContents*(): string =
    assert stateFile.existsFile
    var s = newFileStream(stateFile, fmRead)
    let state = s.readAll
    s.close()
    return state


#-------
# proc insert*(table: var Performance, key: string, val: string) =
#     table[key] = val

proc getAverageElapsedTime*(system: SystemMap, queue: string): uint64 =
    var count: uint32 = 0
    var elapsed: uint64 = 0
    for index in system.jobs.keys: #//TODO: filter?
      if system.jobs[index].queue == queue:
        elapsed += elapsed
    elapsed div count

proc insert*(system: var SystemMap, machine: var Machine) =
    system.machines[machine.id] = machine

proc pullState*(system: var SystemMap) =
    vcsPull(syncUrl)
    system = loadState() 

proc pushState(system: SystemMap) =
    system.storeState("sync")
    vcsPush(syncUrl)  

# proc removeMachine*(id = "") : string =
#     return id

#//TODO: refactor inserts to templates
proc insert*(system: var SystemMap, job: var Job) =
    system.jobs[job.id] = job     

proc availableJob*(map: var SystemMap, sync: bool): (bool, Job ) =
    var returnJob = new Job
    var available = false
    if sync:
        map.pullState()
    if map.jobs.len > 0:
        for index in map.jobs.keys():
            let onejob = map.jobs[index]
            if onejob.state == jsQueued:
                while (onejob.machineId != hostBranchName):
                    onejob.state = jsReserved
                    onejob.machineId = hostBranchName
                    returnJob = onejob
                    available = true
                    map.storeState hostBranchName & " claiming Job: " & returnJob.id
                    if sync:
                        map.pushState()
                        map.pullState()
    return (available, returnJob) 

proc availableWorker*(map: var SystemMap): (bool, Worker) =
    var returnWorker = new Worker
    var available = false
    if map.machines[hostBranchName].workers.len > 0:
        var wrkrs = map.machines[hostBranchName].workers
        for one in wrkrs.keys:
            if wrkrs[one].state == wsIdle:
                returnWorker = wrkrs[one]
                returnWorker.state = wsActive
                available = true
    4.see "Available Worker: " & returnWorker.id
    return (available, returnWorker)

proc begin*(map: var SystemMap, worker: var Worker, job: var Job) =
    job.state = jsActive
    worker.jobId = job.id
    job.workerId = worker.id
    var timer = stopwatch(false)
    timer.start
    map.storeState "Worker: " & worker.id & " started on: " &  job.id
    1.see job.command.runProcess trunkPath
    timer.stop
    job.elapsed = timer.msecs
    worker.state = wsIdle
    job.state = jsComplete
    map.performance[job.queue].updatePerformance(job.elapsed)
    map.storeState "Worker: " & worker.id & " completed on: " &  job.id



# proc removeJob*(id = "") : string =
#     return id

# proc insertWorker*(id = "") : string =
#     return id

# proc removeWorker*(id = "") : string =
#     return id
    

    

