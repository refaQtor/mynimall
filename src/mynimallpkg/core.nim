#core.nim

import os, osproc, ospaths, strutils, tables
import stopwatch
import common
import fossil
import states

var timer = stopwatch(false)

proc init*(project: string, url = ""): SystemMap =
    timer.start()
    project.prepareEnvironment(url)
    var map: SystemMap
    if stateFile.existsFile():
      map.pullState()
      map.storeState "#### RELOADED: "
    else:
      map = newSystemMap()
      map.storeState "#### INITIALIZE: " & project 
    return map

proc hold() =
  while (stdin.readline() != "Q"):
    1.see stateFileContents()
    1.see "\nenter Q to quit"
  #//TODO: remove self from system
  #//TODO: put in proc also called by catching Ctrl-C

proc serve*() =
    let serveProcess = trunkPath.vcsServe
    timer.stop()
    1.see "\n---\nhosting project: " & projectName
    1.see "process id: " & $serveProcess.processID
    1.see "initialized in (msec): " & $timer.totalMsecs
    1.see "open browser on: http://localhost:8080/timeline?v=on&n=50&y=all&t=&ms=exact"
    1.see "enter 'Q' to quit\nor just 'enter' for state update"
    hold()  
    serveProcess.terminate

proc putFiles*(map: SystemMap, fileList: seq[string]) =
    for filename in fileList:
      let infile = filename
      packagesPath.importFile infile

proc workerDoJob(map: var SystemMap, worker: var Worker, job: var Job) =
    map.begin(worker, job)

proc pickAndDo*(map: var SystemMap, sync = false) =
    var (workerAvailable, worker) = map.availableWorker()
    var (jobAvailable, job) = map.availableJob(sync)
    if workerAvailable and jobAvailable:
      map.workerDoJob(worker, job)

proc process*(map: var SystemMap) =
    timer.stop()
    1.see "\n---\nprocessing project: " & projectName
    1.see "initialized in (msec): " & $timer.totalMsecs
    # 1.see "open browser on: http://localhost:8080/timeline?v=on&n=50&y=all&t=&ms=exact"
    # 1.see "enter 'Q' to quit\nor just 'enter' for state update"
    #//TODO: spawn thread to process jobs
    var count = 0
    while (true):
      map.pickAndDo( (syncUrl != "nil") and ((count mod 10) == 0) )
      count += 1
      sleep(250)   #//TDOD: calc/adj sync rate, then sync    
    #//TODO: remove self from system
    #//TODO: put in proc also called by catching Ctrl-C

proc addTask*(system: var SystemMap, queue: string, job: string, interval: uint64 = 0) =
  echo "adding task..."
  var aJob = newJob(queue, job)
  system.jobs[aJob.id] = aJob
  discard system.performance.hasKeyOrPut(queue,newPerformance())
  system.storeState "adding task..." & queue / job
  
proc addMessage*(system: var SystemMap, queue: string, job: string) =
  echo "adding message..."
  system.storeState "adding message..." & queue / job
  
# proc batchJobs*(map: SystemMap, filename: string) =
#   # let newfile = unversionedPath / filename.extractFilename()
#   # filename.copyFileWithPermissions newfile
#     let infile = getCurrentDir() / filename
#     if infile.existsFile:
#       for onejob in infile.lines:
#         map.addTask("batch",onejob)
