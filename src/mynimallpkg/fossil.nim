#fossil.nim
import os, osproc, strutils
import common

const au = "myn"
const ap = "nym"
const pu = "sync"
const pp = "cnys"

const exename = "fossil"
const vcsname = "fsl"
const vcsres = respath / exename
const vcsbin = slurp vcsres 

var fossilExe: string

proc extractFossil() =
    fossilExe = configPath / vcsname
    3.see fossilExe
    if not fossilExe.existsFile:
        fossilExe.writeFile vcsbin
        var perms: set[FilePermission]
        perms = {fpUserExec}
        setFilePermissions(fossilExe, perms)
        assert fossilExe.existsFile
    let exeVer = fossilExe & " version"
    let verOut = exeVer.runProcess
    exeVersions.add newVersion(newConstVersion(
        name=exename,
        tag=verOut[23..26],
        date=verOut[40..49],
        time=verOut[51..58],
        vcs=verOut[28..37],
        extra=fossilExe))

proc vcsCommit*(path, message: string, tag, branch = "")=
    let addremovecmd = fossilExe & " addremove"
    3.see addremovecmd.runProcess path
    let branchArg = if branch == "": "" else: " --branch " & branch
    let tagArg = if tag == "": "" else: " --tag " & tag
    let userArg = " --user " & pu
    let commitcmd = fossilExe &
        " commit -m \"" & message & "\" --no-warnings  --allow-empty --no-prompt" &
        branchArg & tagArg & userArg
    2.see "> " & message
    2.see commitcmd.runProcess path
        
proc vcsOpen(path, basis: string = "") =
    let opencmd = fossilExe & " open " &
        repoDB & " " & basis & " --user " & pu
    discard opencmd.runProcess path

proc setLocalRepoSettings(path: string) =
    let manifestcmd = fossilExe & " setting manifest on"
    discard manifestcmd.runProcess path
    
proc configLocalDb(db: string) =
    let repoarg = " -R " & db 
    let adminUsercmd =  fossilExe &
     " user password " &
     au & " " & ap & repoarg
    discard adminUsercmd.runProcess trunkPath
    let syncoff = fossilExe &
     " settings autosync off" & repoarg
    discard syncoff.runProcess trunkPath
    let projname = fossilExe &
     " sqlite \"insert or replace into config values ('project-name', '" &
      projectName & "', now() );\"" & repoarg
    discard projname.runProcess trunkPath
    trunkPath.vcsOpen
    trunkPath.setLocalRepoSettings
    let syncUsercmd =  fossilExe &
     " user new " & pu & " sync@mynimall.com " & pp
    discard syncUsercmd.runProcess trunkPath
    let capsUsercmd =  fossilExe &
     " user capabilities " & pu & " gioxy"
    discard capsUsercmd.runProcess trunkPath
    let machineUsercmd =  fossilExe &
     " user new " & hostBranchName & " " & hostBranchName &
      "@mynimall.com " & hostBranchName
    discard machineUsercmd.runProcess trunkPath
    let fixmeUsercmd =  fossilExe &
     " user capabilities nobody gi"
    discard fixmeUsercmd.runProcess trunkPath
    let defaultUsercmd =  fossilExe &
     " user default " & pu
    discard defaultUsercmd.runProcess trunkPath
    trunkPath.vcsCommit("root branch db config", rootTag)

proc vcsClone*(url: string) =
    let urlstring = "http://" & url & "/" & projectName &
     " -B " & pu & ":" & pp 
    let clonecmd = fossilExe & " clone " & urlstring &
     " -A myn " & projectName & ".fsl"
    echo clonecmd.runProcess configPath
    configLocalDb(repoDB)

proc vcsPull*(url: string) =
    let urlstring = "http://" & url & "/" & projectName &
        " -B " & pu & ":" & pp 
    let pullcmd = fossilExe & " pull " & urlstring
    echo pullcmd.runProcess trunkPath
    let updatecmd = fossilExe & " update"
    echo updatecmd.runProcess trunkPath

proc vcsPush*(url: string) =
    let urlstring = "http://" & url & "/" & projectName &
        " -B " & pu & ":" & pp 
    let pushcmd = fossilExe & " push " & urlstring &
        " -A myn " & projectName & ".fsl"
    echo pushcmd.runProcess trunkPath
    
proc createCoreBranchPath(branchFolder: string) =
    let repoArg = " -R " & repoDB #//TODO: refactor
    let folder = coreProjectPath / branchFolder
    if not folder.existsDir:   
        let branchcmd = fossilExe &
         " branch new " &
         branchFolder & " " &
         rootTag &
         repoArg &
         " --user " & au
        discard branchcmd.runProcess coreProjectPath
        folder.createDir
    assert folder.existsDir
    let exitdir = getCurrentDir()
    folder.setCurrentDir
    folder.vcsOpen branchFolder
    exitdir.setCurrentDir
    
proc importFile*(branchPath, importFile: string)=
    assert importFile.existsFile
    importFile.copyFileWithPermissions branchPath / importFile.extractFilename
    packagesPath.vcsCommit "adding package: " & importFile
    
# proc vcsClose(path: string)=
#     let closecmd = fossilExe & " close"
#     discard closecmd.runProcess path

proc vcsServe*(path: string): Process =
    startProcess(command = fossilExe,
     workingDir = path,
     args = ["serve"])


proc createNewRepoDB() =
    assert trunkPath != ""
    assert repoDB != ""
    let initcmd = fossilExe &
     " new -A " & au & " " & repoDB 
    discard initcmd.runProcess trunkPath
    assert fileExists repoDB
    configLocalDb(repoDB)
    hostBranchName.createCoreBranchPath
    packagesBranchName.createCoreBranchPath
    packagesPath.importFile getAppFilename()


proc prepareEnvironment*(project: string, url = "") =
    project.setGlobals(url)
    extractFossil()
    if not repoDB.existsFile():
        if url == "nil":
            createNewRepoDB()
        else:
            vcsClone(url)
