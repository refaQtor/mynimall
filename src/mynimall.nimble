# Package

version       = "0.1.0"
author        = "Shannon Mackey"
description   = "Distribute work & harvest data with mynimall effort."
license       = "MPL2"

bin           = @["mynimall"]
bindir        = "bin"

# Dependencies

requires "nim >= 0.17.2"
requires "yaml >= 0.10.3"
requires "docopt >= 0.6.5"
requires "stopwatch >= 3.4"

