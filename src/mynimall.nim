#mynimall.nim
let doc = """
mynimall.

  Distribute work & harvest data with mynimall effort.

  Queue tasks or messages to process concurrently locally, or
  optionally, across distributed computing resources.  
  View system resource status and task activity log in a browser.

Usage:
  mynimall <project> [--distribute | --process] [--sync=<url>] [-b --batch=<file>] [--activity=<verbosity>]
  mynimall <project> <queue> [--execute=<commandline> | --message=<file> | --configure=<file>] [--interval=<seconds>] [--activity=<verbosity>]
  mynimall <project> [--ui]
  mynimall (-h | --help)
  mynimall --version

Options:
  -x --execute=<commandline>  Queue and execute <commandline>.
  -m --message=<file>         Queue and message <file>.
  -b --batch=<file>           Batch load tasks from <file>.
  -d --distribute             Distribute task queues. 
  -s --sync=<url>             Sync task queues via <url>.
  -p --process                Process task queues.
  -i --interval=<seconds>     Repeat job every <seconds> [default: 0].
  -u --ui                     Browse system activity via web ui.
  -a --activity=<verbosity>   Set <verbosity> level [default: 1].   
  -h --help                   Show this screen.
  --version                   Show version.
  
  """

import strutils, parsecsv
import docopt
import mynimallpkg/common
import mynimallpkg/core
import mynimallpkg/states

let args = docopt(doc, version = versionTag)
setVerbosity(parseInt($args["--activity"]))
var systemMap = init($args["<project>"], $args["--sync"])
let exe = $args["--execute"]
1.see "exe: " & exe
let itv = $args["--interval"]
1.see "itv: " & itv
let msg = $args["--message"]
1.see "msg: " & msg
let que = $args["<queue>"]
1.see "que: " & que
let q = if que != "nil" : que else: ""
let bat = $args["--batch"]
1.see "bat: " & bat
let dst = $args["--distribute"]
1.see "dst: " & dst
let pro = $args["--process"]
1.see "pro: " & pro
if bat != "nil":
  var p: CsvParser
  p.open(bat)
  p.readHeaderRow()
  while p.readRow():
    systemMap.addTask(p.rowEntry("queue"), p.rowEntry("command"), 0 )
  p.close()
if exe != "nil":
  systemMap.addTask(q, exe, parseUInt(itv) )
  systemMap.pickAndDo()
if msg != "nil":
  systemMap.addMessage(q, msg)
if parseBool(dst):
  serve() 
if parseBool(pro):
  systemMap.process() 
